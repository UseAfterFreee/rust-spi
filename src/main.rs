use std::{
    error,
    io::{self, prelude::*},
    thread, time,
};

use spidev::{SpiModeFlags, Spidev, SpidevOptions};

fn create_spi() -> io::Result<Spidev> {
    let mut spi = Spidev::open("/dev/spidev0.1")?;
    let options = SpidevOptions::new()
        .bits_per_word(8)
        .max_speed_hz(20_000)
        .mode(SpiModeFlags::SPI_MODE_0)
        .build();
    spi.configure(&options)?;
    Ok(spi)
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let mut spi = create_spi()?;

    loop {
        let mut rx_buf = [0u8; 1];
        spi.read(&mut rx_buf)?;

        println!("{:?}", rx_buf);
        thread::sleep(time::Duration::from_millis(50));
    }
}
